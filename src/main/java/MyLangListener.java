// Generated from /Users/zsomborivanyi/UT/Module8/maven-my-lang/src/main/antlr4/ut/pp/parser/MyLang.g4 by ANTLR 4.12.0
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MyLangParser}.
 */
public interface MyLangListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link MyLangParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(MyLangParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link MyLangParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(MyLangParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link MyLangParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(MyLangParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MyLangParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(MyLangParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MyLangParser#assignment}.
	 * @param ctx the parse tree
	 */
	void enterAssignment(MyLangParser.AssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link MyLangParser#assignment}.
	 * @param ctx the parse tree
	 */
	void exitAssignment(MyLangParser.AssignmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link MyLangParser#if}.
	 * @param ctx the parse tree
	 */
	void enterIf(MyLangParser.IfContext ctx);
	/**
	 * Exit a parse tree produced by {@link MyLangParser#if}.
	 * @param ctx the parse tree
	 */
	void exitIf(MyLangParser.IfContext ctx);
	/**
	 * Enter a parse tree produced by {@link MyLangParser#while}.
	 * @param ctx the parse tree
	 */
	void enterWhile(MyLangParser.WhileContext ctx);
	/**
	 * Exit a parse tree produced by {@link MyLangParser#while}.
	 * @param ctx the parse tree
	 */
	void exitWhile(MyLangParser.WhileContext ctx);
	/**
	 * Enter a parse tree produced by {@link MyLangParser#print}.
	 * @param ctx the parse tree
	 */
	void enterPrint(MyLangParser.PrintContext ctx);
	/**
	 * Exit a parse tree produced by {@link MyLangParser#print}.
	 * @param ctx the parse tree
	 */
	void exitPrint(MyLangParser.PrintContext ctx);
	/**
	 * Enter a parse tree produced by {@link MyLangParser#thread}.
	 * @param ctx the parse tree
	 */
	void enterThread(MyLangParser.ThreadContext ctx);
	/**
	 * Exit a parse tree produced by {@link MyLangParser#thread}.
	 * @param ctx the parse tree
	 */
	void exitThread(MyLangParser.ThreadContext ctx);
	/**
	 * Enter a parse tree produced by {@link MyLangParser#lock}.
	 * @param ctx the parse tree
	 */
	void enterLock(MyLangParser.LockContext ctx);
	/**
	 * Exit a parse tree produced by {@link MyLangParser#lock}.
	 * @param ctx the parse tree
	 */
	void exitLock(MyLangParser.LockContext ctx);
	/**
	 * Enter a parse tree produced by {@link MyLangParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(MyLangParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link MyLangParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(MyLangParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link MyLangParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterLiteral(MyLangParser.LiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link MyLangParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitLiteral(MyLangParser.LiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link MyLangParser#binop}.
	 * @param ctx the parse tree
	 */
	void enterBinop(MyLangParser.BinopContext ctx);
	/**
	 * Exit a parse tree produced by {@link MyLangParser#binop}.
	 * @param ctx the parse tree
	 */
	void exitBinop(MyLangParser.BinopContext ctx);
	/**
	 * Enter a parse tree produced by {@link MyLangParser#ordering}.
	 * @param ctx the parse tree
	 */
	void enterOrdering(MyLangParser.OrderingContext ctx);
	/**
	 * Exit a parse tree produced by {@link MyLangParser#ordering}.
	 * @param ctx the parse tree
	 */
	void exitOrdering(MyLangParser.OrderingContext ctx);
	/**
	 * Enter a parse tree produced by {@link MyLangParser#predicate}.
	 * @param ctx the parse tree
	 */
	void enterPredicate(MyLangParser.PredicateContext ctx);
	/**
	 * Exit a parse tree produced by {@link MyLangParser#predicate}.
	 * @param ctx the parse tree
	 */
	void exitPredicate(MyLangParser.PredicateContext ctx);
	/**
	 * Enter a parse tree produced by {@link MyLangParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(MyLangParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link MyLangParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(MyLangParser.TypeContext ctx);
}