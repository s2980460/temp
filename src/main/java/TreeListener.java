import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class TreeListener extends MyLangBaseListener {
    private ParseTreeProperty<Map<String, Symbol>> symbolmap; // Symbol table


    int x = 0;

    Stack<Symbol> symbolstack = new Stack<Symbol>();

    // Symbol class to store symbol attributes
    private static class Symbol {
        private String type;
        private Object value;
        private Integer scope;

        public Symbol(String type,Integer scope) {
            this.type = type;
            this.scope = scope;
        }

        public String getType() {
            return type;
        }

        public void setValue(Object value) {
            this.value = value;
        }

        public Object getValue() {
            return this.value;
        }

        public Integer getScope() {
            return this.scope;
        }
    }
}